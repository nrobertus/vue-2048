import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist';

Vue.use(Vuex)

const vuexLocalStorage = new VuexPersist({
  key: 'vuex', // The key to store the state on in the storage provider.
  storage: window.localStorage // or window.sessionStorage or localForage
  // Function that passes the state and returns the state with only the objects you want to store.
  // reducer: state => state,
  // Function that passes a mutation and lets you decide if it should update the state in localStorage.
  // filter: mutation => (true)
})

export const store = new Vuex.Store({
  plugins: [vuexLocalStorage.plugin],
  state: {
    r0_c0:0,
    r0_c1:0,
    r0_c2:0,
    r0_c3:0,
    r1_c0:0,
    r1_c1:0,
    r1_c2:0,
    r1_c3:0,
    r2_c0:0,
    r2_c1:0,
    r2_c2:0,
    r2_c3:0,
    r3_c0:0,
    r3_c1:0,
    r3_c2:0,
    r3_c3:0,
    score:0,
    bestScore:0,
    tileTransition: 'slide-fade-up'
  },
  mutations: {
    incrementScore: (state, amount) => {
      state.score = state.score + amount
      if(state.bestScore < state.score){
        state.bestScore = state.score
      }
    },
    clearScore: state => {
      state.score = 0
    },
    setTileTransition: (state, name) => {
      state.tileTransition = name
    },
    setGameStateCell:  (state, props) =>  {
      state['r' + props.row + '_c' + props.col] = props.val
    },
    clearBoard: state => {
      state.r0_c1=0
      state.r0_c0=0
      state.r0_c2=0
      state.r0_c3=0
      state.r1_c0=0
      state.r1_c1=0
      state.r1_c2=0
      state.r1_c3=0
      state.r2_c0=0
      state.r2_c1=0
      state.r2_c2=0
      state.r2_c3=0
      state.r3_c0=0
      state.r3_c1=0
      state.r3_c2=0
      state.r3_c3=0
    }
  },
  getters: {
    score: state => state.score,
    bestScore: state => state.bestScore,
    tileTransition: state => state.tileTransition,
    gameState: state => {
      return [
        [state.r0_c0, state.r0_c1, state.r0_c2, state.r0_c3],
        [state.r1_c0, state.r1_c1, state.r1_c2, state.r1_c3],
        [state.r2_c0, state.r2_c1, state.r2_c2, state.r2_c3],
        [state.r3_c0, state.r3_c1, state.r3_c2, state.r3_c3],
      ]
    },
    zeroCoords: state => {
      let coords = []
      let gameState =  [
        [state.r0_c0, state.r0_c1, state.r0_c2, state.r0_c3],
        [state.r1_c0, state.r1_c1, state.r1_c2, state.r1_c3],
        [state.r2_c0, state.r2_c1, state.r2_c2, state.r2_c3],
        [state.r3_c0, state.r3_c1, state.r3_c2, state.r3_c3],
      ]
      for(var row = 0; row < gameState.length; row++){
        for(var col = 0; col < gameState[row].length; col++){
          if(gameState[row][col] === 0){
            coords.push([row, col])
          }
        }
      }
      return coords
    },
    filledCoords: state => {
      let coords = []
      let gameState =  [
        [state.r0_c0, state.r0_c1, state.r0_c2, state.r0_c3],
        [state.r1_c0, state.r1_c1, state.r1_c2, state.r1_c3],
        [state.r2_c0, state.r2_c1, state.r2_c2, state.r2_c3],
        [state.r3_c0, state.r3_c1, state.r3_c2, state.r3_c3],
      ]
      for(var row = 0; row < gameState.length; row++){
        for(var col = 0; col < gameState[row].length; col++){
          if(gameState[row][col] !== 0){
            coords.push([row, col])
          }
        }
      }
      return coords
    }
  }
})